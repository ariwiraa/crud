package repository

import (
	"context"
	"gits-crud-echo/entity"

	"github.com/google/uuid"

	"github.com/pkg/errors"
	"gorm.io/gorm"
)

type ManagerRepository struct {
	db *gorm.DB
}

func NewManagerRepository(db *gorm.DB) *ManagerRepository {
	return &ManagerRepository{
		db: db,
	}
}

func (repo *ManagerRepository) Insert(ctx context.Context, ent *entity.Manager) error {
	if err := repo.db.
		WithContext(ctx).Model(&entity.Manager{}).Create(ent).Error; err != nil {
		return errors.Wrap(err, "[ManagerRepository-Insert]")
	}

	return nil
}

func (repo *ManagerRepository) GetListManager(ctx context.Context, limit, offset string) ([]*entity.Manager, error) {
	var models []*entity.Manager
	if err := repo.db.
		WithContext(ctx).Model(&entity.Manager{}).Create(&models).Error; err != nil {
		return nil, errors.Wrap(err, "[ManagerRepository-FindAll]")
	}

	return models, nil
}

func (repo *ManagerRepository) GetDetailManager(ctx context.Context, ID uuid.UUID) (*entity.Manager, error) {
	var models *entity.Manager
	if err := repo.db.
		WithContext(ctx).Model(&entity.Manager{}).Take(models, ID).Error; err != nil {
		return nil, errors.Wrap(err, "[ManagerRepository-FindById]")
	}

	return models, nil
}

func (repo *ManagerRepository) UpdateManager(ctx context.Context, ent *entity.Manager) error {
	if err := repo.db.
		WithContext(ctx).Model(&entity.Manager{ManagerId: ent.ManagerId}).Select("manager_name").
		Updates(ent).Error; err != nil {
		return errors.Wrap(err, "[ManagerRepository-Update]")
	}

	return nil
}

func (repo *ManagerRepository) DeleteManager(ctx context.Context, ID uuid.UUID) error {
	if err := repo.db.
		WithContext(ctx).Delete(&entity.Manager{ManagerId: ID}).Error; err != nil {
		return errors.Wrap(err, "[ManagerRepository-Update]")
	}

	return nil
}
