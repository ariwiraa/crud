package http

import (
	"gits-crud-echo/entity"
	"gits-crud-echo/service"
	nethttp "net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

type CreateManagerBodyRequest struct {
	ManagerName string `json:"manager_name" binding:"required"`
}

type ManagerRowResponse struct {
	ManagerId   uuid.UUID `json:"manager_id"`
	ManagerName string    `json:"manager_name"`
}

type ManagerDetailResponse struct {
	ManagerId   uuid.UUID `json:"manager_id"`
	ManagerName string    `json:"manager_name"`
}

type QueryParamsManager struct {
	Limit  string `form:"limit"`
	Offset string `form:"offset"`
	SortBy string `form:"sort_by"`
	Order  string `form:"order"`
	Status string `form:"status"`
}

// func buildManagerRowResponse(manager *entity.Manager) ManagerRowResponse {
// 	form := ManagerRowResponse{
// 		ManagerId:   manager.ManagerId,
// 		ManagerName: manager.ManagerName,
// 	}

// 	return form
// }

// func buildManagerDetailResponse(manager *entity.Manager) ManagerDetailResponse {
// 	form := ManagerDetailResponse{
// 		ManagerId:   manager.ManagerId,
// 		ManagerName: manager.ManagerName,
// 	}

// 	return form
// }

type ManagerHandler struct {
	service service.ManagerUseCase
}

func NewManagerHandler(service service.ManagerUseCase) *ManagerHandler {
	return &ManagerHandler{
		service: service,
	}
}

func (handler *ManagerHandler) CreateManager(echoCtx echo.Context) error {
	var form CreateManagerBodyRequest
	if err := echoCtx.Bind(&form); err != nil {
		return echoCtx.JSON(nethttp.StatusBadRequest, "Tidak ada data")
	}

	managerEntity := entity.NewManager(
		uuid.Nil,
		form.ManagerName,
	)

	if err := handler.service.Create(echoCtx.Request().Context(), managerEntity); err != nil {
		return echoCtx.JSON(nethttp.StatusInternalServerError, "Internal server Error")
	}

	var res = entity.NewResponse(nethttp.StatusCreated, "Create Succesfully", managerEntity)
	return echoCtx.JSON(res.Status, res)

}

func (handler *ManagerHandler) GetListManager(echoCtx echo.Context) error {
	var form QueryParamsManager
	if err := echoCtx.Bind(&form); err != nil {

		return echoCtx.JSON(nethttp.StatusBadRequest, "Tidak ada data")
	}

	manager, err := handler.service.GetListManager(echoCtx.Request().Context(), form.Limit, form.Offset)
	if err != nil {

		return echoCtx.JSON(nethttp.StatusInternalServerError, "tidak ada data")
	}
	var res = entity.NewResponse(nethttp.StatusOK, "Request processed successfully.", manager)
	return echoCtx.JSON(res.Status, res)

}

func (handler *ManagerHandler) GetDetailManager(echoCtx echo.Context) error {
	idParam := echoCtx.Param("id")
	if len(idParam) == 0 {
		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	id, err := uuid.Parse(idParam)
	if err != nil {
		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	manager, err := handler.service.GetDetailManager(echoCtx.Request().Context(), id)
	if err != nil {
		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	var res = entity.NewResponse(nethttp.StatusOK, "Request processed successfully.", manager)
	return echoCtx.JSON(res.Status, res)
}

func (handler *ManagerHandler) UpdateManager(echoCtx echo.Context) error {
	var form CreateManagerBodyRequest
	if err := echoCtx.Bind(&form); err != nil {

		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")

	}

	idParam := echoCtx.Param("id")

	if len(idParam) == 0 {

		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	id, err := uuid.Parse(idParam)
	if err != nil {

		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	_, err = handler.service.GetDetailManager(echoCtx.Request().Context(), id)
	if err != nil {

		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	managerEntity := entity.NewManager(
		uuid.Nil,
		form.ManagerName,
	)

	if err := handler.service.UpdateManager(echoCtx.Request().Context(), managerEntity); err != nil {
		return echoCtx.JSON(nethttp.StatusInternalServerError, "tidak ada data")
	}

	var res = entity.NewResponse(nethttp.StatusOK, "Request processed successfully.", nil)
	return echoCtx.JSON(res.Status, res)
}

func (handler *ManagerHandler) DeleteManager(echoCtx echo.Context) error {
	idParam := echoCtx.Param("id")
	if len(idParam) == 0 {

		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	id, err := uuid.Parse(idParam)
	if err != nil {

		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	err = handler.service.DeleteManager(echoCtx.Request().Context(), id)
	if err != nil {

		return echoCtx.JSON(nethttp.StatusBadRequest, "tidak ada data")
	}

	var res = entity.NewResponse(nethttp.StatusOK, "Request processed successfully.", nil)
	return echoCtx.JSON(res.Status, res)
}
