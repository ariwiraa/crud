package http

import (
	"github.com/labstack/echo/v4"
)

func RunRoutes(managerHandler *ManagerHandler) *echo.Echo {
	e := echo.New()

	e.GET("/", Status)
	e.POST("/create-manager", managerHandler.CreateManager)
	e.GET("/list-manager", managerHandler.GetListManager)
	e.GET("/get-manager/:id", managerHandler.GetDetailManager)
	e.PUT("/update-manager/:id", managerHandler.UpdateManager)
	e.DELETE("/delete-manager/:id", managerHandler.DeleteManager)
	return e
}
