package service

import (
	"context"

	"gits-crud-echo/entity"

	"github.com/google/uuid"
	"github.com/pkg/errors"
)

var (
	ErrNilManager = errors.New("manager is nil")
)

type ManagerService struct {
	managerRepo ManagerRepository
}

func NewManagerService(managerRepo ManagerRepository) *ManagerService {
	return &ManagerService{
		managerRepo: managerRepo,
	}
}

type ManagerUseCase interface {
	Create(ctx context.Context, manager *entity.Manager) error
	GetListManager(ctx context.Context, limit, offset string) ([]*entity.Manager, error)
	GetDetailManager(ctx context.Context, ID uuid.UUID) (*entity.Manager, error)
	UpdateManager(ctx context.Context, ent *entity.Manager) error
	DeleteManager(ctx context.Context, ID uuid.UUID) error
}

type ManagerRepository interface {
	Insert(ctx context.Context, manager *entity.Manager) error
	GetListManager(ctx context.Context, limit, offset string) ([]*entity.Manager, error)
	GetDetailManager(ctx context.Context, ID uuid.UUID) (*entity.Manager, error)
	UpdateManager(ctx context.Context, ent *entity.Manager) error
	DeleteManager(ctx context.Context, ID uuid.UUID) error
}

func (svc ManagerService) Create(ctx context.Context, manager *entity.Manager) error {
	if manager == nil {
		return ErrNilManager
	}

	if manager.ManagerId == uuid.Nil {
		manager.ManagerId = uuid.New()
	}

	if err := svc.managerRepo.Insert(ctx, manager); err != nil {
		return errors.Wrap(err, "[ManajerService-create]")
	}

	return nil

}

func (svc ManagerService) GetListManager(ctx context.Context, limit, offset string) ([]*entity.Manager, error) {
	manager, err := svc.managerRepo.GetListManager(ctx, limit, offset)
	if err != nil {
		return nil, errors.Wrap(err, "[BarangService-FindAll]")
	}

	return manager, nil
}

func (svc ManagerService) GetDetailManager(ctx context.Context, ID uuid.UUID) (*entity.Manager, error) {
	manager, err := svc.managerRepo.GetDetailManager(ctx, ID)
	if err != nil {
		return nil, errors.Wrap(err, "[BarangService-FindByID]")
	}

	return manager, nil
}

func (svc ManagerService) UpdateManager(ctx context.Context, manager *entity.Manager) error {
	if manager == nil {
		return ErrNilManager
	}

	if manager.ManagerId == uuid.Nil {
		manager.ManagerId = uuid.New()
	}

	if err := svc.managerRepo.UpdateManager(ctx, manager); err != nil {
		return errors.Wrap(err, "[ManajerService-create]")
	}

	return nil
}

func (svc ManagerService) DeleteManager(ctx context.Context, ID uuid.UUID) error {
	err := svc.managerRepo.DeleteManager(ctx, ID)
	if err != nil {
		return errors.Wrap(err, "[ManagerService-Create]")
	}
	return nil
}
