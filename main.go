package main

import (
	"fmt"
	"gits-crud-echo/internal/config"
	"gits-crud-echo/internal/handler/http"
	"gits-crud-echo/internal/repository"
	"gits-crud-echo/service"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	var db *gorm.DB
	// migration.Migrate()
	// config.CreateConnection()

	cfg, _ := config.NewConfig(".env")

	db = openDatabase(cfg)

	managerHandler := buildManagerHandler(db)
	e := http.RunRoutes(managerHandler)

	e.Logger.Fatal(e.Start(":8080"))
}

func buildManagerHandler(db *gorm.DB) *http.ManagerHandler {
	repo := repository.NewManagerRepository(db)
	managerService := service.NewManagerService(repo)
	return http.NewManagerHandler(managerService)
}

func openDatabase(config *config.Config) *gorm.DB {
	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.Database.Host,
		config.Database.Port,
		config.Database.Username,
		config.Database.Password,
		config.Database.Name)

	db, _ := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	// checkError(err)
	return db
}
